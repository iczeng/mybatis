package bat.ke.qq.com.statement;
import bat.ke.qq.com.binding.MapperMethod;
import bat.ke.qq.com.result.DefaultResultSetHandler;
import bat.ke.qq.com.result.ResultSetHandler;
import bat.ke.qq.com.session.Configuration;
import bat.ke.qq.com.util.DbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class StatementHandler {
  private Configuration configuration;

  private ResultSetHandler resultSetHandler;

  public StatementHandler() {
  }

  public StatementHandler(Configuration configuration) {
    this.configuration = configuration;
    resultSetHandler=new DefaultResultSetHandler();
  }
  public <T> T query(MapperMethod method, Object parameter) throws Exception {

    Connection connection= DbUtil.open();
    PreparedStatement preparedStatement = connection.prepareStatement(String.format(method.getSql(), Integer.parseInt(String
           .valueOf(parameter))));
    preparedStatement.execute();
    return  resultSetHandler.handle(preparedStatement,method);
  }
}
