#Mybatis
---

  徒手实现mybatis,更加方便我们对mybatis的理解

<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=1c0d93672d39f851223d8ca833ea764d809c3bd15000acb1052d65d89ceeb3e3"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="Java源码交流兵团" title="Java源码交流兵团"></a>

### 技术交流
---
![mybatis](https://gitee.com/ym-monkey/mybatis/raw/master/images/qq.png "qq.png")
* Java互联网技术QQ群：940577921（备注：11）
<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=1c0d93672d39f851223d8ca833ea764d809c3bd15000acb1052d65d89ceeb3e3"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="Java源码交流兵团" title="Java源码交流兵团"></a>
   
### 核心技术
---
* Configuration、 SqlSessionFactory 、Session、 Executor、MappedStatement、StatementHandler、ResultSetHandler
* 名称意义
Configuration	管理mysql-config.xml全局配置关系类
SqlSessionFactory	Session管理工厂接口
Session	SqlSession是一个面向用户（程序员）的接口。SqlSession中提供了很多操作数据库的方法
Executor	执行器是一个接口（基本执行器、缓存执行器）
作用：SqlSession内部通过执行器操作数据库
MappedStatement	底层封装对象
作用：对操作数据库存储封装，包括 sql语句、输入输出参数
StatementHandler	具体操作数据库相关的handler接口
ResultSetHandler	具体操作数据库返回结果的handler接口
![mybatis](https://gitee.com/ym-monkey/mybatis/raw/master/src/test/java/mybatis.png "mybatis.png")